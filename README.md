# Mock Data Generator

A program I quickly hacked together that generates random users and cases.  
Useful for developing UI and UX. Not meant for automated testing.

## Getting started

1. Start the FASoz software stack locally
2. Make sure the backend node environment is set to `development` or `test`
2. Make sure the frontend listens on `localhost:3000`
3. `npm run start`

