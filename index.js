const axios = require("axios");
const faker = require("faker");
const FormData = require("form-data");
const fs = require("fs");

axios.defaults.baseURL = "http://localhost:3000/";
faker.locale = "de";
const numUsers = 80;
const numMaxCasesPerUser = 5;
const numMaxKeywordsPerCase = 5;

function randInt(min, max) {
  return min + Math.floor(Math.random() * (max - min));
}

async function createUser(name, email, password, roles) {
  await axios.post("/api/testing/create_admin", {
    name,
    username: email,
    password,
    roles,
  });
  console.log("User created:", name, email, password, roles);
}

function fakeRoles() {
  const roleSets = [
    ["Administrator", "Editor", "Researcher"],
    ["Editor", "Researcher"],
    ["Researcher"],
    ["Guest"],
  ];
  return roleSets[Math.floor(Math.random() * roleSets.length)];
}

async function main() {
  // reset
  await axios.post("/api/testing/reset");

  // create users
  for (let i = 0; i < numUsers; i++) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email(firstName, lastName);
    const password = faker.internet.password();
    const roles = fakeRoles();

    const promiseCreateUser = createUser(
      `${firstName} ${lastName}`,
      email,
      password,
      roles
    );

    if (!roles.includes("Editor")) continue;

    await promiseCreateUser;

    // create random number of cases for this user
    const numCases = randInt(0, numMaxCasesPerUser);

    if (numCases === 0) continue;

    const loginResponse = await axios.post("/api/auth/login", {
      username: email,
      password,
    });
    const accessToken = loginResponse.data.token;

    for (let j = 0; j < numCases; j++) {
      const title = faker.lorem
        .words(randInt(2, 5))
        .replace(/\w\S*/g, (w) => w.replace(/^\w/, (c) => c.toUpperCase()));
      const description = faker.lorem.sentences(randInt(10, 30));
      const fileCategories = Array.from({ length: randInt(2, 4) }, () =>
        faker.hacker.noun()
      );
      const keywords = faker.lorem
        .words(randInt(4, numMaxKeywordsPerCase))
        .split(" ");
      const participants = Array.from({ length: randInt(1, 8) }, () =>
        faker.animal.bird()
      ).join(", ");
      const pedagogicFields = Array.from({ length: randInt(1, 3) }, () =>
        faker.address.country()
      );
      const problemAreas = Array.from({ length: randInt(2, 5) }, () =>
        faker.commerce.department()
      );

      const createCaseResponse = await axios.post(
        "/api/case",
        {
          title,
          description,
          fileCategories,
          keywords,
          participants,
          pedagogicFields,
          problemAreas,
          published: Boolean(randInt(0, 10) <= 8),
        },
        {
          headers: {
            Authorization: `bearer ${accessToken}`,
          },
        }
      );
      const caseId = createCaseResponse.data.id;
      console.log("Case created:", title, caseId);

      // add random number of files to case
      for (let k = 0; k < randInt(1, 20); k++) {
        const fileCategories = [
          "Komplettakte",
          "Primärdaten",
          "Hintergrundinformationen",
          "Aufgearbeitetes Material",
          "Fachliteratur",
          "Didaktisches Material",
        ];

        const fileCategory = fileCategories[randInt(0, fileCategories.length)];
        const fileName = faker.system.fileName();

        const formData = new FormData();
        formData.append("file", fs.createReadStream("file.txt"), fileName);

        axios
          .post(`/api/case/${caseId}/file/${fileCategory}`, formData, {
            headers: {
              Authorization: `bearer ${accessToken}`,
              ...formData.getHeaders(),
            },
          })
          .then(() =>
            console.log(
              `Uploaded file "${fileCategory}/${fileName}" to case ${caseId}`
            )
          );
      }
    }
  }
}

void main();
